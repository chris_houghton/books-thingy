<?php 
	require_once 'header.php';

	if(!isset($sessionInfo) || empty($sessionInfo)) {
		logout();
		header('Location: index.php');
	}

	$books = getBooksInfo(); 
	$octo = '<img src="octo.gif" alt="*" width="25" height="25">';
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">How to use</div>
			<div class="panel-body">
				You can see a list of all books currently being read on this page. If you wish to add your rating to a book that already exists, click on that book title and you'll be taken to that books page.
				If you wish to add a book that isn't in the list (look first, ctrl+f is your friend), complete the form below the list. Easy.
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<table class="table">
				<thead>
					<tr>
						<th>Title</th>
						<th>Rating</th>
						<th>Readers</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach($books as $book) {
							$rating = "";
							for($i = 0;$i < $book['rating'];$i++) {
								$rating .= $octo;
							}
							printf(
								'<tr>
									<td><a href="book.php?id=%u">%s</a></td>
									<td>%s</td>
									<td>%u</td>
								</tr>',
								$book['book_id'],
								$book['book_title'], 
								$rating, 
								$book['users_rated']
							);
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">Add a Book</div>
			<div class="panel-body">
				<span class="help-block">Make sure you look for it in the list first</span>
				<form action="submit.php" method="post" class="form-horizontal">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
							<input type="text" name="title" placeholder="Title" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="rating" class="col-sm-2 control-label">Rating</label>
						<div class="col-sm-10">
							<input type="text" name="rating" placeholder="Rating" class="form-control">
						</div>
					</div>
					<input type="hidden" name="submit_book" value="1">
					<div class="form-group">
   						<div class="col-sm-offset-2 col-sm-10">
      						<button type="submit" class="btn btn-default">Submit</button>
      					</div>
      				</div>
				</form>
			</div>
		</div>
	</div>
</div>