<?php 
	require_once 'header.php';

	if(!isset($sessionInfo) || empty($sessionInfo)) {
		logout();
		header('Location: index.php');
	}

	if(isset($_GET['id']) && !empty($_GET['id'])) {
		$book = getBookInfo($_GET['id']);
	}

	if(!isset($book) || empty($book)) { ?>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					Doesn't exist!
				</div>
			</div>
		</div>
		<?php 
		exit; 
	}

	$bookExternalInfo = getGoogleBooksInfo($_GET['id'], $book[0]['book_title'], $book[0]['book_isbn'], $book[0]['book_volume_id']);
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php echo $book[0]['book_title']; ?>
			</div>
			<div class="panel-body">
				<?php if(isset($bookExternalInfo) && !empty($bookExternalInfo)) {

					// KABOOM! all those poor authors ;_;
					$authors = implode(', ', $bookExternalInfo->volumeInfo->authors);

					printf('<div class="col-md-3">
								<a href="%s"><img src="%s"></a>
							</div>
							<div class="col-md-9">
								<p>%s</br>
								<strong>%s</strong></p>
							</div>',
							$bookExternalInfo->volumeInfo->infoLink,
							$bookExternalInfo->volumeInfo->imageLinks->thumbnail,
							$bookExternalInfo->volumeInfo->description,
							$authors
						);
				} else {
					'Failed to retrieve Google Books information for this book. You could try adding an ISBN if one doesn\'t exist';
				} ?>	
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<?php printf('Users interested in %s', $book[0]['book_title']); ?>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>Username</th>
						<th>Status</th>
						<th>Rating</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach($book as $reader) {
							printf(
								'<tr>
									<td>%s</td>
									<td>%s</td>
									<td>%u</td>
								</tr>',
								$reader['username'], 
								getStatusString($reader['status']), 
								$reader['rating']
							);
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body">
				<form action="submit.php" method="post" class="form-horizontal">
					<div class="form-group">
						<label for="status" class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10">
							<select name="status" class="form-control">
								<option value="1">Reading</option>
								<option value="2">Planning to Read</option>
								<option value="3">Finished Reading</option>
								<option value="4">Dropped</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="rating" class="col-sm-2 control-label">Rating</label>
						<div class="col-sm-10">
							<input type="text" name="rating" placeholder="Rating" class="form-control">
						</div>
					</div>
					<input type="hidden" name="submit_rating" value="<?php echo $_GET['id']; ?>">
					<input type="hidden" name="user_id" value="<?php echo $sessionInfo['id']; ?>">
					<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
	  						<button type="submit" class="btn btn-default">Submit</button>
	  					</div>
	  				</div>
				</form>
			</div>
		</div>
	</div>
</div>