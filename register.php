<?php 
	require_once 'header.php';

	if(isset($_POST['username']) && !empty($_POST['username'])) {
		if(isset($_POST['password']) && !empty($_POST['password'])) {
			if($_POST['eb_password'] == 'taco420') {
				addUser($_POST['username'],  $_POST['password']);
				header('Location: index.php?success=1');
			}
		}
	}
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body">
				<form method="post" class="form-horizontal">
					<div class="form-group">
						<label for="username" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10">
							<input type="text" name="username" placeholder="username" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" name="password" placeholder="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="eb_password" class="col-sm-2 control-label">EB Password</label>
						<div class="col-sm-10">
							<input type="password" name="eb_password" placeholder="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
	  						<button type="submit" class="btn btn-default">Let's do this</button>
	  					</div>
	  				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
	require_once 'footer.php';