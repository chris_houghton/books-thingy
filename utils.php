<?php
	require_once 'const.php';
	require_once 'phpfastcache.php';

	// Users
	function getSessionInfo() {
		if(!isset($_SESSION['timeout']) || ($_SESSION['timeout'] + SESSION_DURATION < time())) {
			return false;
		}

		if(isset($_SESSION['id']) && isset($_SESSION['session_key'])) {
			return getUserInfo($_SESSION['id'], $_SESSION['session_key']); 
		}

		return false;
	}

	function getUserInfo($sessionId, $sessionKey) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT id, username FROM users WHERE id = ? AND session = ?');
		$stmt->bind_param('is', $sessionId, $sessionKey);
		$stmt->execute();
		$stmt->bind_result($id, $username);
		$stmt->fetch();
		$stmt->close();
		return array('id' => $id, 'username' => $username);
	}

	function addUser($username, $password) {
		$password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
		$date = date("Y-m-d H:i:s");  
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('INSERT INTO users (username, password, date) VALUES (?, ?, ?)');
		$stmt->bind_param('sss', $username, $password, $date);
		$stmt->execute();
		$stmt->close();
	}

	function doLogin($username, $password) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT id, password FROM users WHERE username = ?');
		$stmt->bind_param('s', $username);
		$stmt->execute();
		$stmt->bind_result($userId, $hash);
		$stmt->fetch();
		$stmt->close();
		if(!empty($userId) && password_verify($password, $hash)) {
			$_SESSION['id'] = $userId;
			$_SESSION['session_key'] = makeKey($userId);
			$_SESSION['timeout'] = time();
			header('Location: books.php');
		}
	}

	function changePass($username, $password) {
		$password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('UPDATE users SET password = ? WHERE username = ?');
		$stmt->bind_param('ss', $password, $username);
		$stmt->execute();
		$stmt->close();
	}

	function makeKey($id) {
		$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$query = $db->prepare('UPDATE users SET session = ? WHERE id = ?');
		// Create a random string to be entered into the db
		$key = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$query->bind_param('si', $key, $id);
		$query->execute();
		$query->close();
		return $key;
	}

	// Books

	function getGoogleBooksInfo($id, $title, $isbn = false, $volume = false) {

		/* eventually this will support selecting the right edition from a list returned by the search, 
		but for now we'll just grab the top one */
		$cacheId = "book" . $id;

	    $cache = phpFastCache();

	    // try to get from Cache first.
	    $data = $cache->get($cacheId);

	    if($data == null) {
		    // does the books db info require updating?
			$updateInfo = false;

			$ch = curl_init();
			if(isset($volume) && !empty($volume)) { 
				curl_setopt($ch, CURLOPT_URL, BOOKS_VOLUME_SEARCH . urlencode($volume));
			} elseif(isset($isbn) && !empty($isbn)) {
				curl_setopt($ch, CURLOPT_URL, BOOKS_ISBN_SEARCH . urlencode($isbn));
				$updateInfo = true;
			} else {
				curl_setopt($ch, CURLOPT_URL, BOOKS_TITLE_SEARCH . urlencode($title));
				$updateInfo = true;
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$data = json_decode(curl_exec($ch));

			if(count($data) > 1) {
				$data = $data->items[0];
			}

			if($updateInfo == true) {
					
				// set new info	
				$newIsbn = isset($data->volumeInfo->industryIdentifiers[0]->identifier) ? $data->volumeInfo->industryIdentifiers[0]->identifier : null;
				$newVolume = isset($data->id) ? $data->id : null;

				$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
				$stmt = $mysqli->prepare('UPDATE books SET isbn = ?, volume_id = ? WHERE id = ?');
				$stmt->bind_param('ssi', $newIsbn, $newVolume, $id);
				$stmt->execute();
				$stmt->close();
			}
	
			$cache->set($cacheId, $data, 60*60);
		}
		
		return $data;
	}

	function getLibraryInfo() {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT b.id, b.title, ul.user_id, ul.status, u.username FROM user_library ul INNER JOIN books b ON b.id = ul.book_id  INNER JOIN users u ON u.id = ul.user_id ORDER BY u.date DESC');
		$stmt->execute();
		$stmt->bind_result($bookId, $bookTitle, $userId, $userStatus, $username);
		$books =  array();
		while($stmt->fetch()) {
			$books[] = array('book_id' => $bookId, 'book_title' => $bookTitle, 'user_id' => $userId, 'status' => $userStatus, 'username' => $username);
		}
		$stmt->close();
		return $books;
	}

	function getBookInfo($id) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT b.id, b.title, ul.user_id, ul.status, u.username, ul.rating, b.isbn, b.volume_id FROM user_library ul INNER JOIN books b ON b.id = ul.book_id  INNER JOIN users u ON u.id = ul.user_id WHERE ul.book_id = ? ORDER BY u.date DESC');
		$stmt->bind_param('i', $id);
		$stmt->execute();
		$stmt->bind_result($bookId, $bookTitle, $userId, $userStatus, $username, $rating, $isbn, $volumeId);
		$readers =  array();
		while($stmt->fetch()) {
			$readers[] = array('book_title' => $bookTitle, 'user_id' => $userId, 'status' => $userStatus, 'username' => $username, 'rating' => $rating, 'book_isbn' => $isbn, 'book_volume_id' => $volumeId);
		}
		$stmt->close();
		return $readers;
	}

	function getBooksInfo() {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT id, title, rating, users_rated FROM books');
		$stmt->execute();
		$stmt->bind_result($bookId, $bookTitle, $rating, $usersRated);
		$books =  array();
		while($stmt->fetch()) {
			$books[] = array('book_id' => $bookId, 'book_title' => $bookTitle, 'rating' => $rating, 'users_rated' => $usersRated);
		}
		$stmt->close();
		return $books;
	}

	function addBook($bookTitle, $rating) {
		$usersRated = 1;
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('INSERT INTO books (title, rating, users_rated) VALUES (?, ?, ?)');
		$stmt->bind_param('sii', $bookTitle, $rating, $usersRated);
		$stmt->execute();
		$stmt->close();
	}

	function searchBook($bookTitle) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare("SELECT id, title, rating, users_rated FROM books WHERE title LIKE '%$bookTitle%'");
		$stmt->execute();
		$stmt->bind_result($id, $title, $rating, $users_rated);
		$searchResults = array();
		while($stmt->fetch()) {
			$searchResults[] = array('id' => $id, 'title' => $title, 'rating' => $rating, 'users_rated' => $users_rated);
		}
		if(array_filter($searchResults)) {
			return $searchResults;
		} else {
			return false;
		}
	}

	function updateStatus($bookId, $status, $rating, $userId) {
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT EXISTS(SELECT 1 FROM user_library WHERE book_id = ? AND user_id =?)');
		$stmt->bind_param('ii', $bookId, $userId);
		$stmt->execute();
		$stmt->bind_result($exists);
		$stmt->fetch();
		$stmt->close();

		$date = date("Y-m-d H:i:s");  
		if($exists) {
			$stmt = $mysqli->prepare('UPDATE user_library SET status = ?, rating = ?, date = ? WHERE book_id = ? AND user_id = ?');
			$stmt->bind_param('iisii', $status, $rating, $date, $bookId, $userId);
			$stmt->execute();
			$stmt->close();
			updateRating($bookId, true);
		} else {
			$stmt = $mysqli->prepare('INSERT INTO user_library (status, rating, book_id, user_id, date) VALUES (?, ?, ?, ?, ?)');
			$stmt->bind_param('iiiis', $status, $rating, $bookId, $userId, $date);
			$stmt->execute();
			$stmt->close();
			updateRating($bookId, false);
		}
	}

	function updateRating($bookId, $oldUser) {

		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);
		$stmt = $mysqli->prepare('SELECT users_rated FROM books WHERE id = ?');
		$stmt->bind_param('i', $bookId);
		$stmt->execute();
		$stmt->bind_result($usersRated);
		$stmt->fetch();
		$stmt->close();

		$stmt = $mysqli->prepare('SELECT rating FROM user_library WHERE book_id = ?');
		$stmt->bind_param('i', $bookId);
		$stmt->execute();
		$stmt->bind_result($rating);
		$ratings = array();
		while($stmt->fetch()) {
			$ratings[] = $rating;
		}
		$stmt->close();

		$usersRated = $oldUser ? $usersRated : $usersRated + 1;

		$ratingSum = 0;
		foreach($ratings as $bookRating) {
			$ratingSum += $bookRating;
		}
		$newRating = $ratingSum / $usersRated;

		$stmt = $mysqli->prepare('UPDATE books SET rating = ?, users_rated = ? WHERE id = ?');
		$stmt->bind_param('iii', $newRating, $usersRated, $bookId);
		$stmt->execute();
		$stmt->close();
	}

	function getStatusString($status) {
		switch($status) {
			case 1: 
				return 'Reading';
			case 2:
				return 'Planning to Read';
			case 3:
				return 'Finished Reading';
			case 4:
				return 'Dropped';
			default:
				return 'Who fucking knows?';
		}
	}

	function logout() {
		session_start();
	    session_unset();
	    session_destroy();
	    session_write_close();
	    setcookie(session_name(),'',0,'/');
	    session_regenerate_id(true);
	}

	/**
	 * A Compatibility library with PHP 5.5's simplified password hashing API.
	 *
	 * @author Anthony Ferrara <ircmaxell@php.net>
	 * @license http://www.opensource.org/licenses/mit-license.html MIT License
	 * @copyright 2012 The Authors
	 */

	if (!defined('PASSWORD_BCRYPT')) {

		define('PASSWORD_BCRYPT', 1);
		define('PASSWORD_DEFAULT', PASSWORD_BCRYPT);

		/**
		 * Hash the password using the specified algorithm
		 *
		 * @param string $password The password to hash
		 * @param int    $algo     The algorithm to use (Defined by PASSWORD_* constants)
		 * @param array  $options  The options for the algorithm to use
		 *
		 * @return string|false The hashed password, or false on error.
		 */
		function password_hash($password, $algo, array $options = array()) {
			if (!function_exists('crypt')) {
				trigger_error("Crypt must be loaded for password_hash to function", E_USER_WARNING);
				return null;
			}
			if (!is_string($password)) {
				trigger_error("password_hash(): Password must be a string", E_USER_WARNING);
				return null;
			}
			if (!is_int($algo)) {
				trigger_error("password_hash() expects parameter 2 to be long, " . gettype($algo) . " given", E_USER_WARNING);
				return null;
			}
			switch ($algo) {
				case PASSWORD_BCRYPT:
					// Note that this is a C constant, but not exposed to PHP, so we don't define it here.
					$cost = 10;
					if (isset($options['cost'])) {
						$cost = $options['cost'];
						if ($cost < 4 || $cost > 31) {
							trigger_error(sprintf("password_hash(): Invalid bcrypt cost parameter specified: %d", $cost), E_USER_WARNING);
							return null;
						}
					}
					$required_salt_len = 22;
					$hash_format = sprintf("$2y$%02d$", $cost);
					break;
				default:
					trigger_error(sprintf("password_hash(): Unknown password hashing algorithm: %s", $algo), E_USER_WARNING);
					return null;
			}
			if (isset($options['salt'])) {
				switch (gettype($options['salt'])) {
					case 'NULL':
					case 'boolean':
					case 'integer':
					case 'double':
					case 'string':
						$salt = (string) $options['salt'];
						break;
					case 'object':
						if (method_exists($options['salt'], '__tostring')) {
							$salt = (string) $options['salt'];
							break;
						}
					case 'array':
					case 'resource':
					default:
						trigger_error('password_hash(): Non-string salt parameter supplied', E_USER_WARNING);
						return null;
				}
				if (strlen($salt) < $required_salt_len) {
					trigger_error(sprintf("password_hash(): Provided salt is too short: %d expecting %d", strlen($salt), $required_salt_len), E_USER_WARNING);
					return null;
				} elseif (0 == preg_match('#^[a-zA-Z0-9./]+$#D', $salt)) {
					$salt = str_replace('+', '.', base64_encode($salt));
				}
			} else {
				$buffer = '';
				$raw_length = (int) ($required_salt_len * 3 / 4 + 1);
				$buffer_valid = false;
				if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
					$buffer = mcrypt_create_iv($raw_length, MCRYPT_DEV_URANDOM);
					if ($buffer) {
						$buffer_valid = true;
					}
				}
				if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
					$buffer = openssl_random_pseudo_bytes($raw_length);
					if ($buffer) {
						$buffer_valid = true;
					}
				}
				if (!$buffer_valid && is_readable('/dev/urandom')) {
					$f = fopen('/dev/urandom', 'r');
					$read = strlen($buffer);
					while ($read < $raw_length) {
						$buffer .= fread($f, $raw_length - $read);
						$read = strlen($buffer);
					}
					fclose($f);
					if ($read >= $raw_length) {
						$buffer_valid = true;
					}
				}
				if (!$buffer_valid || strlen($buffer) < $raw_length) {
					$bl = strlen($buffer);
					for ($i = 0; $i < $raw_length; $i++) {
						if ($i < $bl) {
							$buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
						} else {
							$buffer .= chr(mt_rand(0, 255));
						}
					}
				}
				$salt = str_replace('+', '.', base64_encode($buffer));

			}
			$salt = substr($salt, 0, $required_salt_len);

			$hash = $hash_format . $salt;

			$ret = crypt($password, $hash);

			if (!is_string($ret) || strlen($ret) <= 13) {
				return false;
			}

			return $ret;
		}

		/**
		 * Get information about the password hash. Returns an array of the information
		 * that was used to generate the password hash.
		 *
		 * array(
		 *    'algo' => 1,
		 *    'algoName' => 'bcrypt',
		 *    'options' => array(
		 *        'cost' => 10,
		 *    ),
		 * )
		 *
		 * @param string $hash The password hash to extract info from
		 *
		 * @return array The array of information about the hash.
		 */
		function password_get_info($hash) {
			$return = array(
				'algo' => 0,
				'algoName' => 'unknown',
				'options' => array(),
			);
			if (substr($hash, 0, 4) == '$2y$' && strlen($hash) == 60) {
				$return['algo'] = PASSWORD_BCRYPT;
				$return['algoName'] = 'bcrypt';
				list($cost) = sscanf($hash, "$2y$%d$");
				$return['options']['cost'] = $cost;
			}
			return $return;
		}

		/**
		 * Determine if the password hash needs to be rehashed according to the options provided
		 *
		 * If the answer is true, after validating the password using password_verify, rehash it.
		 *
		 * @param string $hash    The hash to test
		 * @param int    $algo    The algorithm used for new password hashes
		 * @param array  $options The options array passed to password_hash
		 *
		 * @return boolean True if the password needs to be rehashed.
		 */
		function password_needs_rehash($hash, $algo, array $options = array()) {
			$info = password_get_info($hash);
			if ($info['algo'] != $algo) {
				return true;
			}
			switch ($algo) {
				case PASSWORD_BCRYPT:
					$cost = isset($options['cost']) ? $options['cost'] : 10;
					if ($cost != $info['options']['cost']) {
						return true;
					}
					break;
			}
			return false;
		}

		/**
		 * Verify a password against a hash using a timing attack resistant approach
		 *
		 * @param string $password The password to verify
		 * @param string $hash     The hash to verify against
		 *
		 * @return boolean If the password matches the hash
		 */
	    function password_verify($password, $hash) {
			if (!function_exists('crypt')) {
				trigger_error("Crypt must be loaded for password_verify to function", E_USER_WARNING);
				return false;
			}
			$ret = crypt($password, $hash);
			if (!is_string($ret) || strlen($ret) != strlen($hash) || strlen($ret) <= 13) {
				return false;
			}

			$status = 0;
			for ($i = 0; $i < strlen($ret); $i++) {
				$status |= (ord($ret[$i]) ^ ord($hash[$i]));
			}

			return $status === 0;
		}
	}