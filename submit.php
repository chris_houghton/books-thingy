<?php
	require_once 'utils.php';
	
	if(isset($_POST['submit_book']) && !empty($_POST['submit_book'])) {
		if(isset($_POST['title']) && !empty($_POST['title']) && isset($_POST['rating']) && !empty($_POST['rating'])) {
			addBook($_POST['title']);
			header('Location: index.php?success=1');
		}
	}

	if(isset($_POST['submit_rating']) && !empty($_POST['submit_rating'])) {
		if(isset($_POST['status']) && !empty($_POST['status']) && isset($_POST['rating']) && !empty($_POST['rating'])) {
			updateStatus($_POST['submit_rating'], $_POST['status'], $_POST['rating'], $_POST['user_id']);
			header('Location: book.php?id='.$_POST['submit_rating'].'&success=1');
		}
	}
